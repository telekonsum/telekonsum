# Telekonsum Commerce 2.x installation profile

Enables the Commerce modules.
Based on Drupal's Standard profile.

Used by [telekonsum/project](https://gitlab.com/telekonsum/project).
